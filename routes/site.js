const express = require("express");
const controller = require("../controller/index");
const bodyParser = require("body-parser");

const router = express.Router();

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({
    extended: true
}));

router
    .get("/", controller.UserController.index)
    .post("/", controller.UserController.add)
    .post("/login", controller.UserController.login)

module.exports = router;
